package com.obsqurazone.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URI;

public class DockerTutorial {

    @Test
    public void formFieldTest(){
        /*
        Variables
         */
        WebDriver driver = null;
        /*
        Set the desired capabilities to get the required browser from the selenium grid
         */
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability(CapabilityType.BROWSER_NAME,"chrome");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-fullscreen");
        caps.setCapability(ChromeOptions.CAPABILITY,  options);
        try {
            driver = new RemoteWebDriver(URI.create("http://localhost:4444/wd/hub").toURL(), caps);
            driver.manage().window().fullscreen();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        // Navigate to the applcation
        driver.navigate().to("http://web/user.html");
        Assert.assertTrue(driver.findElement(By.className("logo")).isDisplayed(), "Logo is not displayed");

        //Fillout the forms
        WebElement userName = driver.findElement(By.name("username"));
        WebElement firstName = driver.findElement(By.name("firstname"));
        WebElement lastName = driver.findElement(By.name("lastname"));
        WebElement address = driver.findElement(By.name("address"));
        WebElement city = driver.findElement(By.name("city"));
        WebElement country = driver.findElement(By.name("country"));
        WebElement post = driver.findElement(By.name("post"));
        WebElement about = driver.findElement(By.name("about"));

        userName.sendKeys("holmes");
        firstName.sendKeys("Shelock");
        lastName.sendKeys("Holmes");
        address.sendKeys("221B Baker Street");
        city.sendKeys("London");
        country.sendKeys("England");
        post.sendKeys("NW1 6XE");
        //Close the browser
        driver.quit();
    }
}
